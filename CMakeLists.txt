project(kio-gemini)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

cmake_minimum_required(VERSION 3.5)
set(QT_MIN_VERSION "5.4.0")
set(KF5_MIN_VERSION "5.38.0")

find_package(ECM ${KF5_MIN_VERSION} REQUIRED COMPONENTS
  KIO
  I18n
  DocTOols)

find_package(KF5DocTools ${KF_DEP_VERSION})

set(CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${ECM_MODULE_PATH}
        ${ECM_KDE_MODULE_DIR}
)

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMSetupVersion)
include(FeatureSummary)

find_package(KF5 ${KF5_MIN_VERSION} REQUIRED KIO I18n)

add_subdirectory(src)

ki18n_install(po)

add_subdirectory(doc)

if(BUILD_TESTING)
  add_subdirectory(autotests)
endif()

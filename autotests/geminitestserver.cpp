// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminitestserver.h"

#include "sslserver.h"

#include <QTcpSocket>
#include <QMutexLocker>
#include <QEventLoop>

GeminiTestServer::GeminiTestServer()
{
  mServer = new SslServer();
  start();
  mReady.acquire();
}

GeminiTestServer::~GeminiTestServer()
{
  wait();
}

quint16 GeminiTestServer::serverPort() const
{
  return mPort;
}

void GeminiTestServer::setResponse(QByteArray response)
{
  QMutexLocker locker(&mMutex);
  mResponse = response;
}

QByteArray GeminiTestServer::request()
{
  QMutexLocker locker(&mMutex);
  return mRequest;
}

void GeminiTestServer::onNewConnection()
{
  auto socket = mServer->nextPendingConnection();
  if (socket) {
    newConnection(socket);
  } else {
    qDebug() << "no socket";
  }
}

bool GeminiTestServer::newConnection(QTcpSocket* socket)
{
   //socket->waitForConnected();

   if (!socket->waitForReadyRead()) {
     qCritical() << "no bytes to read";
   }

   QByteArray buf = socket->readAll();

   QByteArray response;
   {
     QMutexLocker locker(&mMutex);
     mRequest = buf;
     response = mResponse;
   }

//   while (response.size() > 0) {
  if (response.size() > 0) {
     socket->write(response);
     //auto written = socket->write(response);
 //    if (-1 == written || 0 == written) break;
//     response = response.mid(written);
//     socket->waitForBytesWritten();
//   }
   socket->flush();
  }

   socket->disconnectFromHost();
   socket->waitForDisconnected();

   return true;
}

QUrl GeminiTestServer::root() const
{
   return QString::fromLatin1("gemini://localhost:%2/").arg(serverPort());
}


void GeminiTestServer::run()
{
  mServer = new SslServer();


  if (mServer->listen(QHostAddress::LocalHost, 0)) {
    mPort = mServer->serverPort();
    qDebug() << "listening " << root();

    mReady.release();
    QTcpSocket* socket;
    auto numConnections = mMaxConnectionCount;
    for (int i=0; i<numConnections; i++) {
      do {
        mServer->waitForNewConnection(2000);
        socket = mServer->nextPendingConnection();
        if (!socket) {
          qCritical() << "no incomming connection " << i;
          return;
        }
      } while (!newConnection(socket));

      QMutexLocker locker(&mMutex);
      numConnections = mMaxConnectionCount;
      mCurrentConnectionCount = i+1;
    }
  }
}

void GeminiTestServer::setMaxConnectionCount(int count)
{
  QMutexLocker locker(&mMutex);
  mMaxConnectionCount = count;
}

int GeminiTestServer::connectionCount()
{
  QMutexLocker locker(&mMutex);
  return mCurrentConnectionCount;
}



#include "geminitestserver.moc"

#pragma once

#include <QByteArray>
#include <QObject>
#include <QUrl>
#include <QThread>
#include <QMutex>
#include <QSemaphore>

class SslServer;
class QTcpSocket;

class GeminiTestServer : public QThread {
    Q_OBJECT
  public:
      GeminiTestServer();
      ~GeminiTestServer();

      quint16 serverPort() const;

      void setResponse(QByteArray response);
      QByteArray request();

      QUrl root() const;

      void setMaxConnectionCount(int count);
      int connectionCount();

  private:
      void onNewConnection();
      bool newConnection(QTcpSocket* socket);
      void run() override;

      SslServer* mServer;

      QMutex mMutex;
      QSemaphore mReady;
      quint16 mPort;

      QByteArray mResponse;
      QByteArray mRequest;

      int mMaxConnectionCount = 1;
      int mCurrentConnectionCount = 0;
};

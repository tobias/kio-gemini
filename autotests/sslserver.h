#pragma once

#include <QTcpServer>

class SslServer : public QTcpServer {
  Q_OBJECT
  public:
    SslServer(QObject* parent=nullptr);

  protected:
    void incomingConnection(qintptr socketDescriptor) override;
};

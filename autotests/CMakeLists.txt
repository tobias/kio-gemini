include(ECMAddTests)
find_package(Qt5 ${QT_MIN_VERSION} REQUIRED Test Network Widgets)

find_package(KF5 ${KF5_MIN_VERSION} REQUIRED KIO)

add_library(geminitestserver OBJECT geminitestserver.cpp sslserver.cpp)
target_link_libraries(geminitestserver Qt5::Network Qt5::Widgets)

set(gemini_libraries Qt5::Test KF5::KIOCore geminitestserver KF5::KIOWidgets )

ecm_add_test(geminitest.cpp LINK_LIBRARIES ${gemini_libraries})

// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminitestserver.h"

#include <QStandardPaths>
#include <KIO/StoredTransferJob>
#include <KIO/JobUiDelegate>


#include <QSignalSpy>
#include <QTest>


class GeminiTest : public QObject
{
  Q_OBJECT
    private slots:
      void initTestCase();

      void simpleFetch();
      void defaultMimeType();
      void notFound();
      void invalidResponse();
      void redirect();
      void redirectSelfLoop();
      void redirectLoop();

      void input();
      void inputSupported();
};

void GeminiTest::initTestCase()
{
  qputenv("QT_PLUGIN_PATH", QCoreApplication::applicationDirPath().toUtf8());

  QStandardPaths::setTestModeEnabled(true); 
  qputenv("KDE_FORK_SLAVES", "yes");

  KIO::setDefaultJobUiDelegateExtension(nullptr); // no "skip" dialogs
}

void GeminiTest::simpleFetch()
{
  GeminiTestServer server;
  QString responseBody = "foo\nbar\n";
  QString response = "20 text/gemini\r\n" + responseBody;
  server.setResponse(response.toUtf8());

  auto job = KIO::storedGet(server.root());

  QSignalSpy result(job, &KIO::Job::result);
  QVERIFY(result.isValid());
  QSignalSpy mimeType(job, &KIO::TransferJob::mimeTypeFound);
  QVERIFY(mimeType.isValid());

  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(job->exec());

  QVERIFY(!job->error());
  QCOMPARE(job->mimetype(), "text/gemini");
  QCOMPARE(QString::fromUtf8(server.request()), server.root().toString() + "\r\n");
  QCOMPARE(QString::fromUtf8(job->data()), responseBody);

  QCOMPARE(mimeType.count(), 1);
  QCOMPARE(mimeType.takeFirst().at(1), "text/gemini");
  QCOMPARE(result.count(), 1);

  QCOMPARE(job->queryMetaData("ssl_in_use"), "TRUE");
}

void GeminiTest::defaultMimeType()
{
  GeminiTestServer server;
  QString responseBody = "foo\nbar\n";
  QString response = "20 \r\n" + responseBody;
  server.setResponse(response.toUtf8());

  auto job = KIO::storedGet(server.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(job->exec());

  QVERIFY(!job->error());
  QCOMPARE(job->mimetype(), "text/gemini");
  QCOMPARE(QString::fromUtf8(server.request()), server.root().toString() + "\r\n");
  QCOMPARE(QString::fromUtf8(job->data()), responseBody);
  QCOMPARE(job->queryMetaData("charset"), "utf-8");
}

void GeminiTest::notFound()
{
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("51 Not Found\r\n"));

  auto job = KIO::storedGet(server.root());

  QSignalSpy result(job, &KIO::Job::result);
  QVERIFY(result.isValid());

  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);

  QVERIFY(!job->exec());
  QVERIFY(job->error());
  QCOMPARE(job->error(), KIO::ERR_DOES_NOT_EXIST);
  QCOMPARE(result.count(), 1);
}

void GeminiTest::invalidResponse()
{
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("nope"));

  auto job = KIO::storedGet(server.root());
  
  QSignalSpy result(job, &KIO::Job::result);
  QVERIFY(result.isValid());

  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);

  QVERIFY(!job->exec());
  QVERIFY(job->error());
  QCOMPARE(result.count(), 1);
}

void GeminiTest::redirect()
{
  GeminiTestServer server2;
  server2.setResponse(QByteArrayLiteral("20 text/gemini\r\nfound"));
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("30 ").append(server2.root().toString().toUtf8()).append("\r\n"));

  auto job = KIO::storedGet(server.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(job->exec());
  QVERIFY(!job->error());
  QCOMPARE(QString::fromUtf8(server.request()), server.root().toString() + "\r\n");
  QCOMPARE(QString::fromUtf8(server2.request()), server2.root().toString() + "\r\n");
  QCOMPARE(QString::fromUtf8(job->data()), "found");
  QCOMPARE(job->url(), server2.root());
}

void GeminiTest::redirectSelfLoop()
{
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("30 ").append(server.root().toString().toUtf8()) + "\r\n");

  auto job = KIO::storedGet(server.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(!job->exec());
  QCOMPARE(QString::fromUtf8(server.request()), server.root().toString() + "\r\n");
}

void GeminiTest::redirectLoop()
{
  GeminiTestServer server1;
  server1.setMaxConnectionCount(3);

  GeminiTestServer server2;
  server2.setMaxConnectionCount(3);
  server1.setResponse(QByteArrayLiteral("30 ").append(server2.root().toString().toUtf8()) + "\r\n");
  server2.setResponse(QByteArrayLiteral("30 ").append(server1.root().toString().toUtf8()) + "\r\n");

  auto job = KIO::storedGet(server1.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  
  QVERIFY(!job->exec());
  QCOMPARE(QString::fromUtf8(server1.request()), server1.root().toString() + "\r\n");
  QCOMPARE(QString::fromUtf8(server2.request()), server2.root().toString() + "\r\n");

  QCOMPARE(server1.connectionCount() + server2.connectionCount(), 5);
}

void GeminiTest::input()
{
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("10 prompt\r\n"));

  auto job = KIO::storedGet(server.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(!job->exec());
  QCOMPARE(job->error(), KIO::ERR_UNSUPPORTED_PROTOCOL);
}

void GeminiTest::inputSupported()
{
  GeminiTestServer server;
  server.setResponse(QByteArrayLiteral("10 prompt\r\n"));

  auto job = KIO::storedGet(server.root());
  job->addMetaData("ssl_no_ui", "TRUE");
  job->addMetaData("inputSupported", "TRUE");
  job->setUiDelegate(nullptr);
  QVERIFY(job->exec());
  QCOMPARE(job->error(), 0);
  QCOMPARE(job->queryMetaData("input"), "prompt");
  QCOMPARE(job->mimetype(), "text/gemini");

}

QTEST_MAIN(GeminiTest);

#include "geminitest.moc"

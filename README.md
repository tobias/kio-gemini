# KIO Gemini
Provides transparent access to files using the Gemini protocol.

You probably want to use this in conjunction with the geminipart and Konqueror
for browsing and maybe Akregator for fetching atom feeds.

Supports most of the Gemini Protocol with the exception of client certificates
and certificate trust (TOFU); i.e.: All certificates are accepted.

// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <kio/tcpslavebase.h>
#include <optional>

class QSslSocket;
class QSslError;

class Gemini : public QObject, public KIO::TCPSlaveBase
{
  Q_OBJECT
  public:
    Gemini(const QByteArray &pool, const QByteArray &app);
    void get(const QUrl& url) override;
    void mimetype(const QUrl& url) override;


  private:
    virtual void write(const QByteArray &data) override;

    bool openUrl(const QUrl& url);

    void writeLine(const QByteArray &data);
    QString readLine();
    QByteArray readData();

    QByteArray mBuffer;

    bool sendRequest(const QUrl& url);
    struct Header {
      int code[2];
      QString meta;
    };
    std::optional<Header> getHeader();

    int mRedirectCount = 0;

    QSslSocket* sslSocket();

  private slots:
    void onSslErrors(const QList<QSslError>& errors);
};

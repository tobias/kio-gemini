// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "gemini.h"
#include <gemini-debug.h>

#include <QDebug>
#include <QCoreApplication>
#include <KLocalizedString>
#include <QSslSocket>

static const QByteArray crlf = QByteArrayLiteral("\r\n");
static constexpr int lineMaxLength = 1024;
static constexpr int gemini_default_port = 1965;

static constexpr int gemini_median_file_size = 1500; // gemini://gemini.bortzmeyer.org/software/lupa/stats.gmi

static constexpr bool ignoreSslCertErrors = true;


// https://invent.kde.org/frameworks/kio/-/blob/master/docs/metadata.txt
//
// export QT_LOGGING_RULES="kf5.kio.gemini=true"

class KIOPluginForMetaData : public QObject
{
  Q_OBJECT
    Q_PLUGIN_METADATA(IID "kio.slave.gemini" FILE "gemini.json")
};

extern "C"
{
  int Q_DECL_EXPORT kdemain(int argc, char **argv)
  {
    QCoreApplication app(argc, argv); // needed for password server
    app.setApplicationName(QStringLiteral("kio_gemini"));

    qInfo() << "Launching KIO slave.";
    if (argc != 4) {
      fprintf(stderr, "Usage: kio_gemini protocol domain-socket1 domain-socket2\n");
      exit(-1);
    }
    Gemini slave(argv[2], argv[3]);
    slave.dispatchLoop();
    return 0;
  }
}


Gemini::Gemini(const QByteArray &pool, const QByteArray &app)
  : TCPSlaveBase("Gemini", pool, app, true)
{
  mBuffer.reserve(2*gemini_median_file_size);
}


void Gemini::write(const QByteArray &data)
{
  qDebug(KIO_GEMINI) << "write" << data;
  int bytesWritten = 0;
  while (bytesWritten < data.length()) {
    auto r = KIO::TCPSlaveBase::write(data.data()+bytesWritten, data.length()-bytesWritten);
    qDebug(KIO_GEMINI) << "writen" << r;
    if (r < 0) return; //FIXME ERROR?
    bytesWritten += r;
  }
}

void Gemini::writeLine(const QByteArray &data)
{
  // prevents errors in servers that do not wait for the second package
  write(data + crlf);
  /*
   // do not use this
  write(data);
  write(crlf);
  */
}


QString Gemini::readLine()
{
  const auto maxLength = 3 + 1024 + 2;
  int bytesRead = mBuffer.size();
  mBuffer.resize(lineMaxLength);
  while (bytesRead < maxLength) {
    auto n = read(mBuffer.data() + bytesRead, mBuffer.size() - bytesRead);
    qDebug(KIO_GEMINI) << "read: " << n;
    if (n <= 0) {
      return QString();
    }
    bytesRead += n;
    if (auto i = mBuffer.left(bytesRead).indexOf(crlf); i >= 0) {
      qDebug(KIO_GEMINI) << "length: " << i;
      auto line = QString::fromUtf8(mBuffer.data(), i);
      mBuffer.resize(bytesRead);
      mBuffer.remove(0, i+crlf.size());

      return line;
    }
  }
  return QString();
}

QByteArray Gemini::readData()
{
  qDebug(KIO_GEMINI) << "read data";
  constexpr int bufferSizeIncrement = 2 * gemini_median_file_size;
  int totalSize = mBuffer.size();
  if (mBuffer.size() > 0) {
    processedSize(totalSize);
    data(mBuffer);
  }
  mBuffer.resize(bufferSizeIncrement);
  int ret;
  while ((ret = read(mBuffer.data(), mBuffer.length())) > 0) {
    qDebug(KIO_GEMINI) << "read" << ret;
    totalSize += ret;
    processedSize(totalSize);
    mBuffer.resize(ret);
    data(mBuffer);
    mBuffer.resize(bufferSizeIncrement);
  }

  return {};
}

QSslSocket* Gemini::sslSocket()
{
  return qobject_cast<QSslSocket*>(this->socket());
}

void Gemini::onSslErrors(const QList<QSslError>& /*errors*/)
{
  qDebug(KIO_GEMINI) << "QSslError";
  sslSocket()->ignoreSslErrors();
}

bool Gemini::openUrl(const QUrl &url)
{
  mBuffer.clear();

  int port = url.port(gemini_default_port);
  infoMessage(QString("connecting to: %1:%2").arg(url.host()).arg(port));
  qDebug(KIO_GEMINI) << "connecing:"<< url.host() << port;

  /*
   // does not work since TCPSLaveBase ingores ssl error anyway and only checks
   // later, which we can not overwrite
  connect(sslSocket(), QOverload<const QList<QSslError> &>::of(&QSslSocket::sslErrors),
          this, &Gemini::onSslErrors);
          */

  if (ignoreSslCertErrors) {
    sslSocket()->setPeerVerifyMode(QSslSocket::VerifyNone);
  }

  if (!connectToHost("gemini", url.host(), port)) return false;

  qDebug(KIO_GEMINI) << "blocking";
  setBlocking(true);

  if (!isUsingSsl()) {
    qWarning(KIO_GEMINI) << "ssl failed";
    error(KIO::ERR_CANNOT_CONNECT, i18n("SSL failed"));
    return false;
  }

  return true;
}

bool Gemini::sendRequest(const QUrl &url)
{
  infoMessage("sending request");
  writeLine(url.url().toUtf8());

  return true;
}

std::optional<Gemini::Header> Gemini::getHeader()
{
  infoMessage("waiting for response");
  const auto response = readLine();
  infoMessage("header received");
  qDebug(KIO_GEMINI) << "response" << response << "\n\n";
  if (response.length() < 3) {
    error(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid response"));
    return {};
  }

  if (!response[0].isDigit() || !response[1].isDigit() ||
      response[2] != ' ') {
    error(KIO::ERR_CONNECTION_BROKEN, i18n("Invalid header"));
    return {};
  }

  return {{
    response[0].digitValue(),
    response[1].digitValue(),
    response.mid(3)
  }};
}

void Gemini::get(const QUrl &url)
{
  mimetype(url);
  qDebug(KIO_GEMINI) << "connected" << isConnected();
  if (!isConnected()) return;


  infoMessage("receiving data");
  data(readData());

  disconnectFromHost();
  infoMessage("data received");
  finished();
}

void Gemini::mimetype(const QUrl&	url) 	
{
  qDebug(KIO_GEMINI) << "Mimetype" << url;
  if(!openUrl(url)) return;

  if(!sendRequest(url)) {
    disconnectFromHost();
    return;
  }

  const auto ret = getHeader();
  if (!ret) {
    disconnectFromHost();
    return;
  }
  auto header = *ret;

  setMetaData("responsecode", QString::number(header.code[0]*10 + header.code[1]));


  if (header.code[0] == 1) {
    if (header.code[1] == 1) {
      if (hasMetaData("no-auth")) {
        error(KIO::ERR_ACCESS_DENIED, QStringLiteral("error authentication required"));
        disconnectFromHost();
      }

      KIO::AuthInfo authInfo;
      authInfo.url = url;
      authInfo.prompt = header.meta;
      authInfo.setExtraField("hide-username-line", true);
      //authInfo.setExtraField("bypass-cache-and-kwallet", true);
      authInfo.readOnly = true;
      authInfo.keepPassword = false;

      if (!checkCachedAuthentication(authInfo)) {
        int errorCode = openPasswordDialogV2(authInfo);
        if (errorCode) {
          switch (errorCode) {
            case KIO::ERR_USER_CANCELED:
              error(errorCode, QStringLiteral("authentication canceled"));
              break;
            case KIO::ERR_PASSWD_SERVER:
              // ERR_ACCESS_DENIED?? FIXME
              error(errorCode, QStringLiteral("internal error: communication with password server failed"));
              break;
            default:
              error(errorCode, QStringLiteral("error with authentication"));
          }
          disconnectFromHost();
          return;
        }
      }
      QUrl rUrl(url);
      rUrl.setQuery(authInfo.password);
      redirection(rUrl);
      disconnectFromHost();
      finished();
      return;
    } else {
      setMetaData("input", header.meta);
      // send a "fake" mimetype such that we get the metadata
      mimeType("text/gemini");
      if (!hasMetaData("inputSupported")) {
        error(KIO::ERR_UNSUPPORTED_PROTOCOL, QStringLiteral("Gemini INPUT"));
      } else {
        finished();
      }
      disconnectFromHost();
      return;
    }
  }

  if (header.code[0] != 2)  {
    switch (header.code[0]) {
      case 1:
        error(KIO::ERR_UNSUPPORTED_PROTOCOL, QStringLiteral("Gemini INPUT"));
        break;
      case 3:
        qDebug(KIO_GEMINI) << "redirect" << header.meta << " count: " << mRedirectCount;
        {
          QUrl redirectUrl(header.meta);
          if (!redirectUrl.isValid()) {
            error(KIO::ERR_DOES_NOT_EXIST, redirectUrl.toString());
            break;
          }

          if (redirectUrl.isRelative()) {
            redirectUrl = url.resolved(redirectUrl);
          }

          if (redirectUrl == url) {
            error(KIO::ERR_CYCLIC_LINK, redirectUrl.toString());
            disconnectFromHost();
            return;
          } else {
            mRedirectCount++;
            if (mRedirectCount >= 5) {
              auto result = messageBox(WarningContinueCancel,
                                       "Redirection limit reached; continue anyway?",
                                       "Redirection");
              if (result != Continue) {
                qDebug(KIO_GEMINI) << "redirection canceled";
                error(KIO::ERR_USER_CANCELED, redirectUrl.toString());
                disconnectFromHost();
                mRedirectCount = 0;
                return;
              } else {
                qDebug(KIO_GEMINI) << "redirection continue";
              }
            }
            if (header.code[1] == 1) {
              setMetaData("permanent-redirect", "true");
            }
            redirection(redirectUrl);
            disconnectFromHost();
            finished();
            return;
          }
        }
        break;
      case 4:
        switch (header.code[1]) {
          case 1:
          case 2:
            error(KIO::ERR_INTERNAL_SERVER, header.meta);
            break;
          default:
            error(KIO::ERR_CANNOT_STAT, url.toString());
            break;
        }
        break;
      case 5:
        error(KIO::ERR_DOES_NOT_EXIST, url.toString());
        break;
      case 6:
        error(KIO::ERR_ACCESS_DENIED, url.toString());
        break;
      default:
        error(KIO::ERR_CONNECTION_BROKEN, url.toString());
    }
    disconnectFromHost();
    return;
  }
  mRedirectCount = 0;

  if (header.meta.isEmpty()) {
    qDebug(KIO_GEMINI) << "using default mimeType";
    mimeType("text/gemini");
    setMetaData("charset", "utf-8");
  } else {
    auto headerParts = header.meta.split(';');
    for (int i = 1; i < headerParts.size(); i++) {
      auto d = headerParts[i].split('=');
      if (d.size() == 2) {
        const auto key = d[0].trimmed().toLower();
        if ("charset" == key) {
          setMetaData(key, d[1].trimmed());
        } else if ("lang" == key) {
          setMetaData("content-language", d[1].trimmed()); // same as http kio
        }
      }
    }

    qDebug(KIO_GEMINI) << "using mimeType" << headerParts[0];
    // set mimeType and send metaData
    mimeType(headerParts[0]);
  }
}

#include "gemini.moc"

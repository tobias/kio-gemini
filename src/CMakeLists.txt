include(ECMQtDeclareLoggingCategory)

set(gemini_SRCS gemini.cpp)

ecm_qt_declare_logging_category(gemini_SRCS
    IDENTIFIER KIO_GEMINI
    HEADER gemini-debug.h
    CATEGORY_NAME kf.kio.gemini
    DESCRIPTION "KIO Gemini Slave"
    EXPORT KIO)

# https://community.kde.org/Guidelines_and_HOWTOs/Making_apps_run_uninstalled
# need at least 5.38 for INSTALL_NAMESPACE
kcoreaddons_add_plugin(kio_gemini SOURCES ${gemini_SRCS}
  JSON gemini.json
  INSTALL_NAMESPACE kf5/kio)
target_link_libraries(kio_gemini KF5::KIOCore KF5::I18n)
set_target_properties(kio_gemini PROPERTIES OUTPUT_NAME gemini)
